pragma optional_param default_ssl_check_cert 1;
C!
W!
# Backends

backend F_fastlytest_www_cio_com {
    .first_byte_timeout = 15s;
    .connect_timeout = 1s;
    .max_connections = 200;
    .between_bytes_timeout = 10s;
    .share_key = "4RfX9VTE5mw40xVt55WQiG";
    .port = "80";
    .host = "70.42.185.51";
  
      
  
}
backend F_apollo_sitemaps_s3 {
    .connect_timeout = 1s;
    .dynamic = true;
    .port = "80";
    .host = "apollo-sitemaps.s3-website-us-east-1.amazonaws.com";
    .first_byte_timeout = 15s;
    .max_connections = 200;
    .between_bytes_timeout = 10s;
    .share_key = "4RfX9VTE5mw40xVt55WQiG";
  
      
    .probe = {
        .request = "HEAD / HTTP/1.1"  "Host: apollo-sitemaps.s3-website-us-east-1.amazonaws.com" "Connection: close";
        .window = 5;
        .threshold = 1;
        .timeout = 2s;
        .initial = 5;
        .dummy = true;
      }
}








# custom acl (access control list)
## included in top of main

# Who is allowed access ...
acl goodguys {
    "localhost";
    "38.99.32.0"/24; /* 501 Second */
    "206.80.3.0"/26; /* 501 Second */
    "206.80.4.64"/26; /* 501 Second */
    "70.42.185.0"/24; /* 365 Main */
    "23.235.32.0"/20;	/* FASTLY */
    "43.249.72.0"/22;	/* FASTLY */
    "103.244.50.0"/24;	/* FASTLY */
    "103.245.222.0"/23;	/* FASTLY */
    "103.245.224.0"/24;	/* FASTLY */
    "104.156.80.0"/20;	/* FASTLY */
    "157.52.64.0"/18;	/* FASTLY */
    "185.31.16.0"/22;	/* FASTLY */
    "199.27.72.0"/21;	/* FASTLY */
    "202.21.128.0"/24;	/* FASTLY */
    "203.57.145.0"/24;	/* FASTLY */
}



table s3_sitemap {
    "www.fritterati.com": "frit",
    "www.itnews.com": "itn",
    "fastlytest.www.cio.com": "itn",
  }


sub vcl_recv {



  # default conditions
  set req.backend = F_fastlytest_www_cio_com;
      
  
     if (!req.http.Fastly-FF) {
       if (req.http.X-Forwarded-For) {
         set req.http.Fastly-Temp-XFF = req.http.X-Forwarded-For ", " client.ip;
       } else {
         set req.http.Fastly-Temp-XFF = client.ip;
       }
     } else {
       set req.http.Fastly-Temp-XFF = req.http.X-Forwarded-For;
     }
      


  set req.grace = 60s; 
  set req.http.host = "fastlytest.www.cio.com";      

  
  # end default conditions

  

          
  # The above line causes the Fastly macros to be added.  Do not remove!


if( req.url ~ "^/(seo/sitemap.*|sitemap.xml$)" && req.url != "/seo/sitemap/news" ) {
  set req.url = "/" table.lookup(s3_sitemap,req.http.host) req.url;
  set req.http.host = "apollo-sitemaps.s3-website-us-east-1.amazonaws.com";
  set req.backend = F_apollo_sitemaps_s3;
}


  # Custom Cookie Detection
## Included in vcl_recv

# limit all access via goodguys acl
#  if (client.ip ! goodguys) {
#    error 403 "Forbidden";  }


  # Custom Argument Whitelist
## Included in vcl_recv


## sort arguments to increase cache hits
if(req.url ~ "(\?|&)") {
set req.url = boltsort.sort(req.url);
}


####################
## Blacklist all for select URLs
####################
# IDG.tv needs start= for the homepage
#if(req.url ~ "^/(\?|&).*") {
#set req.url = regsuball(req.url, ".*", "/");
#return(lookup);
#} #end if

####################
## Whitelist all for select URLs
####################

if(req.url ~ "(\?|&)") {
	if(req.url ~ "^/(godigital|ads\/bing|techpapers\/submit|napi)") {
		return(lookup);
	} #end if
} #end if

####################
## Whitelist all other urls
####################
## only evaluate if the url has an argument
if(req.url ~ "(\?|&)") {

# properly removes all arguments that are not in the whitelist
set req.url = regsuball(req.url, "([\?|&])(?!((?i)(action|aid|ajax|ajaxSearchType|APIClientID|APIVersion|author|bc|brandId|blxContentId|c|callback|caller|categoryId|categorySelect|catId|catSlug|cds_record_status|ch|chld|cid|collection|companyId|contentClass|contentType|ct|d|days|dc|dct|def|delay|displayId|displayTypeId|divId|dt|email|emailadd|EMAIL|emailAddr|endDate|f|featureClass|filters|fname|full|google_editors_picks|hackHTML|height|id|ids|iframe|init|isBroll|jobTitle|m|maxPrice|maxRows|minPrice|minWeight|mobileQuery|mode|mt|nl_name|NotLoggedIn|nsdr|page|pageNumber|pageSize|pageType|pagination|partnerCategory.catzeroId|partnerCategory.pageId|partnerCategory.topcatId|password|passwordConfirm|pid|platform|pretty|priceRange|prodid|productId|q|qt|query|region|resourceTypeId|reviewID|rid|rtypeSelect|s|searchType|sectionID|size|sort|sortBy|sortOrder|source|sourceid|sponsor|sponsorSelect|ss|start|startDate|startIndex|style|submit|submitForm|submitted|subsource|t|tag|threadId|timestamp|token|type|typeId|typeID|url|usercaptcha_response|username|userType|v|width|yearsInIt|zpt)(=|&|$)))[^&]+", "\1");

####################
## Cleanup
####################
# leaves orphaned &s & ?s so we clean up below
# reduce multiple &s
set req.url = regsuball(req.url, "&+", "&");
# fix ?& to ?
set req.url = regsuball(req.url, "\?&", "\?");
# strip trailing & or ?
set req.url = regsuball(req.url, "[\?|&]+$", "");

# test added arg
#set req.url = regsuball(req.url, "(.*)", "\1?sean");

return(lookup);

} # end if


## clean version
##set req.url = regsuball(req.url, "([\?|&])(?!((foo|bar|yo)(=|&|$)))[^&]+", "\1");

## regex explained:
#
# (match arg start & or ?)
#([\?|&])
#
# do not match args that are foo follwed by =,& or end of the arg
# the (?i) makes them case insensitive
#(?!((?i)(foo|bar|yo)(=|&|$)))
#
# do match everything else that isn't an &
#[^&]+
#
# replace the match with the first captured ? or & 
#"\1"


  # Custom rules for rss feeds
  # include "rss.vcl";
    
  ## custom vcl to cover URL patterns to always pass
## included in vcl_recv
#  These are items that were originally in ttl.vcl file
#  They have been pulled out and placed here.  By having them in vcl_recv we get a little better performance since
#  we know we are not going to cache them.  ALso - make sure you do return(pass), NOT return(deliver) here.


## Pass to origin, do not cache
	if (req.url ~ "^\/newsletters\/nl_module_processor.*") {return (pass);}
	if (req.url ~ "^\/godigital/") {return (pass);}
	if (req.url ~ "^\/ads\/bing.*") {return (pass);}
	if (req.url ~ "^\/cds\/PrePopGatewayResponse.do") {return (pass);}
	if (req.url ~ "^\/techpapers\/submit(/?$|/.+)") {return (pass);}
	if (req.url ~ "^\/user(/?$|/.+)") {return (pass);}
	if (req.url ~ "^\/api\/registration\.json.*") {return (pass);}
	if (req.url ~ "^\/articleComment\/get\.do.*?[?|&]username\="){return (pass);}

##### IDGE code	

	if (req.url ~ "^\/captcha\/.*") {return (pass);}
	if (req.url ~ "^\/insider\/token.*") {return (pass);}
	if (req.url ~ "^/fb-instant-articles.rss") {return (pass);}
	if (req.url ~ "/.fb-instant-articles.rss") {return (pass);}

### NARF code
if (req.url ~ "^\/techpapers\/submit.*/")  {return (pass);}
if (req.url ~ "^\/articleComment\/get\.do.*?[?|&]username\=/")  {return (pass);}
if (req.url ~ "^\/newsletters\/nl_module_processor.*/")  {return (pass);}
if (req.url ~ "^\/user\/.*/")  {return (pass);}
if (req.url ~ "^\/captcha\/.*/")  {return (pass);}
if (req.url ~ "^\/ads\/bing.*/")  {return (pass);}
if (req.url ~ "^\/cds\/PrePopGatewayResponse.do/")  {return (pass);}
if (req.url ~ "^\/api\/registration\.json.*/")  {return (pass);}
if (req.url ~ "^\/godigital/")  {return (pass);}


  # This puts back in the ACL for purging.  EAB 02/01/2016
  if (req.request == "FASTLYPURGE" /* check that the request is a purge */
    && !(client.ip ~ goodguys)) {  /* and that the requesting IP is not within the ACL */
       error 403 "Access Denied";
   }
  if (req.request != "HEAD" && req.request != "GET" && req.request != "FASTLYPURGE") {
    return(pass);
  }
    return(lookup);
}


sub vcl_fetch {



#--FASTLY FETCH BEGIN


# record which cache ran vcl_fetch for this object and when
  set beresp.http.Fastly-Debug-Path = "(F " server.identity " " now.sec ") " if(beresp.http.Fastly-Debug-Path, beresp.http.Fastly-Debug-Path, "");

# generic mechanism to vary on something
  if (req.http.Fastly-Vary-String) {
    if (beresp.http.Vary) {
      set beresp.http.Vary = "Fastly-Vary-String, "  beresp.http.Vary;
    } else {
      set beresp.http.Vary = "Fastly-Vary-String, ";
    }
  }
  
    
  
 # priority: 0

      
    # no-cache 404/403
    set beresp.ttl = 0s;
    set beresp.grace = 0s;
    return(pass);
  
 
      
  # Header rewrite insider private header : 10
  
      
        set beresp.http.cache-control = beresp.http.cache-control;
              
  
 
 
    
 # priority: 10
 if ( req.url ~ "^/insider"|| req.http.cookie ~ "nsdr"|| req.url ~ "nsdr=true" ) {
  
        
      # insider pass
      set beresp.ttl = 0s;
      set beresp.grace = 0s;
      return(pass);
    
 
 
 
  }
      
#--FASTLY FETCH END

# The above line causes the Fastly macros to be added.  Do not remove!

# Remove all cookies from origin response
unset beresp.http.Set-Cookie;

 # If we get an error and it is our first attempt to get file from origin then try one more time
 if ((beresp.status == 500 || beresp.status == 503) && req.restarts < 1 && (req.request == "GET" || req.request == "HEAD")) {
   restart;
 }

 if(req.restarts > 0 ) {
   set beresp.http.Fastly-Restarts = req.restarts;
 }

 if (beresp.http.Set-Cookie) {
   set req.http.Fastly-Cachetype = "SETCOOKIE";
   return (pass);
 }

 if (beresp.http.Cache-Control ~ "private") {
   set req.http.Fastly-Cachetype = "PRIVATE";
  set beresp.ttl = 0s;
   set beresp.grace = 0s;
   # Switched back to return(pass) - with return(deliver) and ttl=0 you risk data leakage.  Request collapsing can 
   # cause one user to see another user's response.  By having return(pass) this won't happen. EAB (2/1/2016)
   #return (deliver);
   return (pass);
 }

 if (beresp.status == 500 || beresp.status == 503) {
   set req.http.Fastly-Cachetype = "ERROR";
   set beresp.ttl = 1s;
   set beresp.grace = 5s;
   return (deliver);
 }

  if (beresp.http.Expires || beresp.http.Surrogate-Control ~ "max-age" || beresp.http.Cache-Control ~"(s-maxage|max-age)") {
    # keep the ttl here
  } else {
    # apply the default ttl
    set beresp.ttl = 14400s;
  }

  ## custom ttl
## included in vcl_fetch

#set Surrogate-Key header for cache clearing by AID
if(req.url ~ ".*video\?id=.*") {
  set beresp.http.Surrogate-Key = regsuball(req.url, ".*id=([0-9]+).*", "\1" ) ;
  set beresp.http.X-Surrogate-Key = beresp.http.Surrogate-Key ;
}


if (parse_time_delta(subfield(beresp.http.Edge-Control, "downstream-ttl")) >= 0) {
  set beresp.http.Cache-Control = "max-age=" parse_time_delta(subfield(beresp.http.Edge-Control, "downstream-ttl"));
  return (deliver);
}
if (parse_time_delta(subfield(beresp.http.Edge-Control, "cache-maxage")) >= 0) {
  set beresp.ttl = parse_time_delta(subfield(beresp.http.Edge-Control, "cache-maxage"));
  return (deliver);
}
if (parse_time_delta(subfield(beresp.http.Surrogate-Control, "max-age")) >= 0) {
  set beresp.ttl = parse_time_delta(subfield(beresp.http.Cache-Control, "max-age"));
  return (deliver);
}
if (parse_time_delta(subfield(beresp.http.Cache-Control, "s-maxage")) >= 0) {
  #set beresp.ttl = parse_time_delta(subfield(beresp.http.Cache-Control, "max-age"));
  # Konstantin:
  #Typo above?  Shouldn't max-age be s-maxage? (EAB 2/1/2016)
  set beresp.ttl = parse_time_delta(subfield(beresp.http.Cache-Control, "s-maxage"));
  return (deliver);
}
if (parse_time_delta(subfield(beresp.http.Cache-Control, "max-age")) >= 0) {
  set beresp.ttl = parse_time_delta(subfield(beresp.http.Cache-Control, "max-age"));
  return (deliver);
}



##########################################################################
## Pass to origin, do not cache
# MOVED to no_cache.vcl (EAB 02/01/2016)
	##if (req.url ~ "^\/newsletters\/nl_module_processor.*") {return (pass);}
	##if (req.url ~ "^\/godigital/") {return (pass);}
	##if (req.url ~ "^\/ads\/bing.*") {return (pass);}
	##if (req.url ~ "^\/cds\/PrePopGatewayResponse.do") {return (pass);}
	##if (req.url ~ "^\/techpapers\/submit(/?$|/.+)") {return (pass);}
	##if (req.url ~ "^\/user(/?$|/.+)") {return (pass);}
	##if (req.url ~ "^\/api\/registration\.json.*") {return (pass);}
	##if (req.url ~ "^\/articleComment\/get\.do.*?[?|&]username\="){return (pass);}
## End changes by EAB

##### IDGE code	

	##if (req.url ~ "^\/captcha\/.*") {return (pass);} ## Moved to no_cache.vcl EAB 02/01/2016
	if (req.url ~ "^\/search\/.*") { set beresp.ttl = 600s; set beresp.grace = 0s; return (deliver);}
	if (req.url ~ "^\/(news|reviews|blogs|howto|column|browse|video|category)\/(?!.(start=))/") { set beresp.ttl = 600s; set beresp.grace = 0s; return (deliver);}
	if (req.url ~ "^\/(column|blog)\/.?(start=([0-9]|[0-9][0-9]|100)(?![0-9]))/") { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
	if (req.url ~ "^\/(news|reviews|blogs|howto|browse|video|category)\/.?(start=([0-9]|[0-9][0-9]|100)(?![0-9]))/") { set beresp.ttl = 600s; set beresp.grace = 0s; return (deliver);}
	if (req.url ~ "^\/seo\/sitemap\/news/") { set beresp.ttl = 600s; set beresp.grace = 0s; return (deliver);}
	if (req.url ~ "^\/category\/.*(\/|\/index.html)?/") { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
	if (req.url ~ "^\/(howto|reviews|news|resources)(\/|\/index.html)?/") { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
	if (req.url ~ "^\/www(\..+)?\/./") { set beresp.ttl = 86400s; set beresp.grace = 0s; return (deliver);}
	if (req.url ~ "^\/feeds\/partner\/.*/") { set beresp.ttl = 300s; set beresp.grace = 0s; return (deliver);}
	if (req.url ~ "/.index.rss/") { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
	if (req.url ~ "^\/category\/(hardware|security).*") { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
	if (req.url ~ "^\/question\/unanswered.*") { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
	##if (req.url ~ "^\/insider\/token.*") {return (pass);} ## Moved to no_cache.vcl EAB 02/01/2016
	##if (req.url ~ "^/fb-instant-articles.rss") {return (pass);} ## Moved to no_cache.vcl EAB 02/01/2016
	##if (req.url ~ "/.fb-instant-articles.rss") {return (pass);} ## Moved to no_cache.vcl EAB 02/01/2016

### NARF code
if (req.url == "/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "/.*\=v\_product\_price.*/")  { set beresp.ttl = 2678401s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/flyout.*/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/network.*/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/search.*/")  { set beresp.ttl = 600s; set beresp.grace = 0s; return (deliver);}
##if (req.url ~ "^\/techpapers\/submit.*/")  {return (pass);} ## Moved to no_cache.vcl EAB 02/01/2016
##if (req.url ~ "^\/articleComment\/get\.do.*?[?|&]username\=/")  {return (pass);} ## Moved to no_cache.vcl EAB 02/01/2016
if (req.url ~ "^\/articleComment\/.*/")  { set beresp.ttl = 60s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/newsletters\/nl_module_processor.*/")  {return (pass);} ## Moved to no_cache.vcl EAB 02/01/2016
##if (req.url ~ "^\/user\/.*/")  {return (pass);} ## Moved to no_cache.vcl EAB 02/01/2016
##if (req.url ~ "^\/captcha\/.*/")  {return (pass);} ## Moved to no_cache.vcl EAB 02/01/2016
##if (req.url ~ "^\/ads\/bing.*/")  {return (pass);} ## Moved to no_cache.vcl EAB 02/01/2016
##if (req.url ~ "^\/cds\/PrePopGatewayResponse.do/")  {return (pass);} ## Moved to no_cache.vcl EAB 02/01/2016
if (req.url ~ "^\/(news|reviews|howto|column|browse|video|category)\/(?!.*(start=))/")  { set beresp.ttl = 600s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/(column)\/.*?(start=([0-9]|[0-9][0-9]|100)(?![0-9]))/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/(news|reviews|howto|browse|video|category)\/.*?(start=([0-9]|[0-9][0-9]|100)(?![0-9]))/")  { set beresp.ttl = 600s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/product\/pg\/directory(\/|\/.html)?/")  { set beresp.ttl = 86400s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/product\/pg\/.*/")  { set beresp.ttl = 86400s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/product\/directory\/.*/")  { set beresp.ttl = 2678400s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/seo\/sitemap\/news/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/seo\/sitemap\//")  { set beresp.ttl = 604800s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/phones(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/tablets(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/cameras(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/apps(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/web-social(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/entertainment(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/gadgets(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/business(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/laptop-computers(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/computers-all(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/printers(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/security(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/software(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/mobile(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/windows(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/macs(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/os-x(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/business(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/mac-apps(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/mobile(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/ios-apps(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/entertainment(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/category\/creative(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/howto(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/reviews(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/news(\/|\/index.html)?/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/www(\..+)?\/.*/")  { set beresp.ttl = 0s; set beresp.grace = 86400s; return (deliver);}
##if (req.url ~ "^\/api\/registration\.json.*/")  {return (pass);} ## Moved to no_cache.vcl (EAB 02/01/2016)
if (req.url ~ "^\/api\/.*/")  { set beresp.ttl = 1800s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "^\/feeds\/partner\/.*/")  { set beresp.ttl = 300s; set beresp.grace = 0s; return (deliver);}
if (req.url ~ "/.*index.rss/")  { set beresp.ttl = 900s; set beresp.grace = 0s; return (deliver);}
##if (req.url ~ "^\/godigital/")  {return (pass);} ## Moved to no_cache.vcl (EAB 02/01/2016)



##########################################################################
# allow override for testing
 # priority: 10
 #if ( req.http.User-Agent == "Yo Gabba Gabba" ) {
#	set beresp.ttl = 5s;
#	set beresp.http.X-fastly-ttl = beresp.ttl ;
#	set beresp.grace = 1800s;
#	set beresp.http.X-fastly-stale = beresp.grace ;
#	return(deliver);
 # }
  
        
        
##########################################################################        
## Default if nothing set by origin

if ( beresp.http.Cache-Control !~ ".*max-age\=.*" ) {
	set beresp.ttl = 14400s;
	set beresp.grace = 5d;
	## wherever grace in vcl_fetch change to ttl stale if error (grace needs to be set in vcL_recv ALSO)
	# set headers
	set beresp.http.Cache-Control = "max-age=" regsuball(beresp.ttl, "\.[0-9]+", ""); # strip decimals from ttl
	set beresp.http.X-fastly-ttl = beresp.ttl ;
	set beresp.http.X-fastly-stale = beresp.grace ;
	return(deliver);
}

# else set the stale header
#	set beresp.grace = 2678400s;
#	set beresp.http.X-fastly-stale = beresp.grace ;


  return(deliver);
}

sub vcl_hit {
#--FASTLY HIT BEGIN

# we cannot reach obj.ttl and obj.grace in deliver, save them when we can in vcl_hit
  set req.http.Fastly-Tmp-Obj-TTL = obj.ttl;
  set req.http.Fastly-Tmp-Obj-Grace = obj.grace;

  {
    set req.http.Fastly-Cachetype = "HIT";

    
  }
#--FASTLY HIT END
# The above line causes the Fastly macros to be added.  Do not remove!


  if (!obj.cacheable) {
    return(pass);
  }
  return(deliver);
}

sub vcl_miss {
#--FASTLY MISS BEGIN
  

# this is not a hit after all, clean up these set in vcl_hit
  unset req.http.Fastly-Tmp-Obj-TTL;
  unset req.http.Fastly-Tmp-Obj-Grace;

  {
    if (req.http.Fastly-Check-SHA1) {
       error 550 "Doesnt exist";
    }
    
#--FASTLY BEREQ BEGIN
    {
      if (req.http.Fastly-Original-Cookie) {
        set bereq.http.Cookie = req.http.Fastly-Original-Cookie;
      }
      
      if (req.http.Fastly-Original-URL) {
        set bereq.url = req.http.Fastly-Original-URL;
      }
      {
        if (req.http.Fastly-FF) {
          set bereq.http.Fastly-Client = "1";
        }
      }
      {
        # do not send this to the backend
        unset bereq.http.Fastly-Original-Cookie;
        unset bereq.http.Fastly-Original-URL;
        unset bereq.http.Fastly-Vary-String;
        unset bereq.http.X-Varnish-Client;
      }
      if (req.http.Fastly-Temp-XFF) {
         if (req.http.Fastly-Temp-XFF == "") {
           unset bereq.http.X-Forwarded-For;
         } else {
           set bereq.http.X-Forwarded-For = req.http.Fastly-Temp-XFF;
         }
         # unset bereq.http.Fastly-Temp-XFF;
      }
    }
#--FASTLY BEREQ END


 #;

    set req.http.Fastly-Cachetype = "MISS";

    
  }
#--FASTLY MISS END
# The above line causes the Fastly macros to be added.  Do not remove!
  return(fetch);
}

sub vcl_deliver {




#--FASTLY DELIVER BEGIN

# record the journey of the object, expose it only if req.http.Fastly-Debug.
  if (req.http.Fastly-Debug || req.http.Fastly-FF) {
    set resp.http.Fastly-Debug-Path = "(D " server.identity " " now.sec ") "
       if(resp.http.Fastly-Debug-Path, resp.http.Fastly-Debug-Path, "");

    set resp.http.Fastly-Debug-TTL = if(obj.hits > 0, "(H ", "(M ")
       server.identity
       if(req.http.Fastly-Tmp-Obj-TTL && req.http.Fastly-Tmp-Obj-Grace, " " req.http.Fastly-Tmp-Obj-TTL " " req.http.Fastly-Tmp-Obj-Grace " ", " - - ")
       if(resp.http.Age, resp.http.Age, "-")
       ") "
       if(resp.http.Fastly-Debug-TTL, resp.http.Fastly-Debug-TTL, "");

    set resp.http.Fastly-Debug-Digest = digest.hash_sha256(req.digest);
  } else {
    unset resp.http.Fastly-Debug-Path;
    unset resp.http.Fastly-Debug-TTL;
  }

  # add or append X-Served-By/X-Cache(-Hits)
  {

    if(!resp.http.X-Served-By) {
      set resp.http.X-Served-By  = server.identity;
    } else {
      set resp.http.X-Served-By = resp.http.X-Served-By ", " server.identity;
    }

    set resp.http.X-Cache = if(resp.http.X-Cache, resp.http.X-Cache ", ","") if(fastly_info.state ~ "HIT($|-)", "HIT", "MISS");

    if(!resp.http.X-Cache-Hits) {
      set resp.http.X-Cache-Hits = obj.hits;
    } else {
      set resp.http.X-Cache-Hits = resp.http.X-Cache-Hits ", " obj.hits;
    }

  }

  if (req.http.X-Timer) {
    set resp.http.X-Timer = req.http.X-Timer ",VE" time.elapsed.msec;
  }

  # VARY FIXUP
  {
    # remove before sending to client
    set resp.http.Vary = regsub(resp.http.Vary, "Fastly-Vary-String, ", "");
    if (resp.http.Vary ~ "^\s*$") {
      unset resp.http.Vary;
    }
  }
  unset resp.http.X-Varnish;


  # Pop the surrogate headers into the request object so we can reference them later
  set req.http.Surrogate-Key = resp.http.Surrogate-Key;
  set req.http.Surrogate-Control = resp.http.Surrogate-Control;

  # If we are not forwarding or debugging unset the surrogate headers so they are not present in the response
  if (!req.http.Fastly-FF && !req.http.Fastly-Debug) {
    unset resp.http.Surrogate-Key;
    unset resp.http.Surrogate-Control;
  }

  if(resp.status == 550) {
    return(deliver);
  }
  

  #default response conditions
    
  
          

  
#--FASTLY DELIVER END
# The above line causes the Fastly macros to be added.  Do not remove!

  return(deliver);
}

sub vcl_error {
#--FASTLY ERROR BEGIN

  if (obj.status == 801) {
     set obj.status = 301;
     set obj.response = "Moved Permanently";
     set obj.http.Location = "https://" req.http.host req.url;
     synthetic {""};
     return (deliver);
  }

  
          
  if (req.http.Fastly-Restart-On-Error) {
    if (obj.status == 503 && req.restarts == 0) {
      restart;
    }
  }

  {
    if (obj.status == 550) {
      return(deliver);
    }
  }
#--FASTLY ERROR END


# The above line causes the Fastly macros to be added.  Do not remove!
# custom error
## included in vcl_error

# Look for cookie and send to auth if not present
 if (obj.status == 750) {
                set obj.http.location = req.http.Location;
                set obj.status = 302;
                return (deliver);
        }


## Test code
# if (obj.status == 751) {
#set obj.http.X-Debug = obj.response ;
#    set obj.http.Content-Type = "text/html; charset=utf-8";
#    
#    synthetic {"
#<?xml version="1.0" encoding="utf-8"?>
#<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
# "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
#<html>
#  <head>
#    <title>"} obj.status " " obj.response {"</title>
#  </head>
#  <body>
#    <h1>Error "} obj.status " " obj.response {"</h1>
#    <p>"} obj.response {"</p>
#    <h3>Guru Meditation:</h3>
#    <p>XID: "} req.xid {"</p>
#    <hr>
#    <p>Varnish cache server</p>
#  </body>
#</html>
#"};
#    return (deliver);
#
#  }

}

sub vcl_pass {
#--FASTLY PASS BEGIN
  

  {
    
#--FASTLY BEREQ BEGIN
    {
      if (req.http.Fastly-Original-Cookie) {
        set bereq.http.Cookie = req.http.Fastly-Original-Cookie;
      }
      
      if (req.http.Fastly-Original-URL) {
        set bereq.url = req.http.Fastly-Original-URL;
      }
      {
        if (req.http.Fastly-FF) {
          set bereq.http.Fastly-Client = "1";
        }
      }
      {
        # do not send this to the backend
        unset bereq.http.Fastly-Original-Cookie;
        unset bereq.http.Fastly-Original-URL;
        unset bereq.http.Fastly-Vary-String;
        unset bereq.http.X-Varnish-Client;
      }
      if (req.http.Fastly-Temp-XFF) {
         if (req.http.Fastly-Temp-XFF == "") {
           unset bereq.http.X-Forwarded-For;
         } else {
           set bereq.http.X-Forwarded-For = req.http.Fastly-Temp-XFF;
         }
         # unset bereq.http.Fastly-Temp-XFF;
      }
    }
#--FASTLY BEREQ END


 #;
    set req.http.Fastly-Cachetype = "PASS";
  }

#--FASTLY PASS END
# The above line causes the Fastly macros to be added.  Do not remove!
}

sub vcl_log {
  # Custom Log
  # TEST REMARK
#  fastly service ID instead!!!!!



# Custom Log
 log {"syslog 7gqBivD7zzkCdQFy5vwDR0 syslog.use :: "} req.http.Fastly-Client-IP {" ["} now {"] "} req.request {" "} req.http.Host {" "} req.url {" "} resp.status {" ["}  req.http.referer {"] ["} req.http.User-Agent {"] "} resp.http.Content-Length {" "} resp.http.X-Cache {" "} resp.http.Age {" "}  geoip.region  {" "} resp.http.X-Served-By ;

log {"syslog 7gqBivD7zzkCdQFy5vwDR0 syslog.use :: "} req.http.Fastly-Client-IP {"   Zooooty!!"} ;

}

sub vcl_pipe {
#--FASTLY PIPE BEGIN
  {
     
    
#--FASTLY BEREQ BEGIN
    {
      if (req.http.Fastly-Original-Cookie) {
        set bereq.http.Cookie = req.http.Fastly-Original-Cookie;
      }
      
      if (req.http.Fastly-Original-URL) {
        set bereq.url = req.http.Fastly-Original-URL;
      }
      {
        if (req.http.Fastly-FF) {
          set bereq.http.Fastly-Client = "1";
        }
      }
      {
        # do not send this to the backend
        unset bereq.http.Fastly-Original-Cookie;
        unset bereq.http.Fastly-Original-URL;
        unset bereq.http.Fastly-Vary-String;
        unset bereq.http.X-Varnish-Client;
      }
      if (req.http.Fastly-Temp-XFF) {
         if (req.http.Fastly-Temp-XFF == "") {
           unset bereq.http.X-Forwarded-For;
         } else {
           set bereq.http.X-Forwarded-For = req.http.Fastly-Temp-XFF;
         }
         # unset bereq.http.Fastly-Temp-XFF;
      }
    }
#--FASTLY BEREQ END


    #;
    set req.http.Fastly-Cachetype = "PIPE";
    set bereq.http.connection = "close";
  }
#--FASTLY PIPE END

}

sub vcl_hash {

  #--FASTLY HASH BEGIN

    
  
  #if unspecified fall back to normal
  {
    

    set req.hash += req.url;
    set req.hash += req.http.host;
    set req.hash += "#####GENERATION#####";
    return (hash);
  }
  #--FASTLY HASH END


}

